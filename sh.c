// Shell.

#include "types.h"
#include "user.h"
#include "fcntl.h"
#include "stat.h"

// Parsed command representation
#define EXEC  1
#define REDIR 2
#define PIPE  3
#define LIST  4
#define BACK  5

#define MAXARGS 10
#define PATH_MAX 256
#define IOCTL_CLEAR 7

char* env[21];
char* envv[21];

struct cmd {
  int type;
};

struct execcmd {
  int type;
  char *argv[MAXARGS];
  char *eargv[MAXARGS];
};

struct redircmd {
  int type;
  struct cmd *cmd;
  char *file;
  char *efile;
  int mode;
  int fd;
};

struct pipecmd {
  int type;
  struct cmd *left;
  struct cmd *right;
};

struct listcmd {
  int type;
  struct cmd *left;
  struct cmd *right;
};

struct backcmd {
  int type;
  struct cmd *cmd;
};

//fd for any user-provided script
//by default 0 for stdin (see fgets
//and gets in ulib.c for details)
int scriptfd = 0;

int fork1(void);  // Fork but panics on failure.
void panic(char*);
struct cmd *parsecmd(char*);

int
getenv(char* name, char* buffer)
{
  for (int i = 0; env[i][0] != 0; ++i) {
    slice(buffer, env[i], indexof(env[i], "="));
    if (strcmp(name, buffer) == 0) {
      slice(buffer, &env[i][indexof(env[i], "=")+1], strlen(env[i]));
      return 1;
    }
  }
  buffer[0] = 0;
  return 0;
}

int
setenv(char* input)
{
  int i = 0;
  int length = strlen(input);
  char name[length];
  char buffer[length - indexof(input, "=")];

  slice(name, input, indexof(input, "="));
  slice(buffer, &input[indexof(input, "=")+1], length);

  for (; env[i][0] != 0; ++i) {
    char testname[length];
    slice(testname, env[i], indexof(env[i], "="));
    if (strcmp(name, testname) == 0) {
      strncpy(env[i], input, strlen(input));
      strcpy(&env[i][length], "\0");

      return 0;
    }
  }
  strncpy(env[i], input, strlen(input));
  strcpy(&env[i][length+1], "\0");
  strcpy(env[i+1], "\0");

  return 0;
}

void setenvironment(int second) {
  int i = 0;
  for(; i < 21; ++i) {
    env[i] = malloc(50);
    memset(env[i], 0, 50);
  }
  if (second) {
    for(i=0; i < 21; ++i) {
      envv[i] = malloc(50);
      memset(envv[i], 0, 50);
    }
  }

  if (second) {
    setenv("path=/:");
    strcpy(&env[2][0], "\0");
  }
}

//expand environment variables if any
void expandenv(char **argv, char** newarg) {
  int i, j;

  for(j=0; argv[j]; j++)
    ;

  for(i = 0; i < j; i++) {
    newarg[i] = malloc(strlen(argv[i])+1);
    strcpy(newarg[i], argv[i]);
    if (argv[i][0] == '$') {
      char buffer[100];
      getenv(argv[i]+1, buffer);
      newarg[i] = malloc(strlen(buffer)+1);
      strcpy(newarg[i], buffer);
      newarg[i][strlen(buffer)] = 0;
    }
  }
  newarg[i] = 0;

}

void pathexec(char *argv[]) {
  if (argv[0][0] != '/') {
    int len = strlen(env[0]);
    char path[len];
    slice(path, &env[0][indexof(env[0], "=")+1], len);
    while(path[0] != 0) {
      char output[strlen(path)];
      slice(output, path, indexof(path, ":")); //"output" is the path to test
      char TMPPATH[512];
      strncpy(TMPPATH, output, 256); //concat cwd to path
      if (TMPPATH[strlen(TMPPATH)-1] != '/')
        strncat(TMPPATH, "/", 1);
      strncpy(TMPPATH + strlen(TMPPATH), argv[0], 256);

      exec(TMPPATH, argv, env);

      slice(path, &path[indexof(path, ":")+1], len);
    }
  }
}

// Execute cmd.  Never returns.
void
runcmd(struct cmd *cmd)
{
  int p[2];
  char* newarg[50];
  struct backcmd *bcmd;
  struct execcmd *ecmd;
  struct listcmd *lcmd;
  struct pipecmd *pcmd;
  struct redircmd *rcmd;
  struct stat *redirStats = 0;
  int return_code;

  if(cmd == 0)
    exit(-1);

  switch(cmd->type){
  default:
    panic("runcmd");


  case EXEC:
  {
    ecmd = (struct execcmd*)cmd;
    if(ecmd->argv[0] == 0)
      exit(1);

    expandenv(ecmd->argv, newarg);

    int i = 0;
    for (;strcmp(&env[i][0], "\0") != 0; ++i);
      ;
    env[i] = 0;
    exec(newarg[0], newarg, env); //exec should never return

    //if exec fails, try PATH
    pathexec(newarg);
    break;
  }
  case REDIR:
    rcmd = (struct redircmd*)cmd;
    close(rcmd->fd);
    int stat_res = stat(rcmd->file, redirStats);
    if(stat_res > 0 && redirStats->type == T_DEV) {
      rcmd->mode &= ~O_CREATE;  // Don't recreate a device file!
    }
    int fd = open(rcmd->file, rcmd->mode);
    if(fd < 0){
      printf(2, "open %s failed\n", rcmd->file);
      exit(2);
    }
    runcmd(rcmd->cmd);
    break;

  case LIST:
    lcmd = (struct listcmd*)cmd;
    if(fork1() == 0)
      runcmd(lcmd->left);
    wait(&return_code);
    runcmd(lcmd->right);
    break;

  case PIPE:
    pcmd = (struct pipecmd*)cmd;
    if(pipe(p) < 0)
      panic("pipe");
    if(fork1() == 0){
      close(1);
      dup(p[1]);
      close(p[0]);
      close(p[1]);
      runcmd(pcmd->left);
    }
    if(fork1() == 0){
      close(0);
      dup(p[0]);
      close(p[0]);
      close(p[1]);
      runcmd(pcmd->right);
    }
    close(p[0]);
    close(p[1]);
    wait(&return_code);
    wait(&return_code);
    break;

  case BACK:
    bcmd = (struct backcmd*)cmd;
    if(fork1() == 0)
      runcmd(bcmd->cmd);
    break;
  }
  exit(0);
}

int
getcmd(char *buf, int nbuf)
{
  static char cwd[PATH_MAX];
  getcwd(cwd);

  if(scriptfd == 0) { printf(2, "%s $ ", cwd); }
  memset(buf, 0, nbuf);
  fgets(scriptfd, buf, nbuf); //see gets and fgets in ulib.c
  if(buf[0] == 0) // EOF
    return -1;
  return 0;
}

int
main(int argc, char* argv[])
{
  static char buf[100];
  int fd;
  int return_code;

  // Ensure that three file descriptors are open.
  while((fd = open("console", O_RDWR)) >= 0){
    if(fd >= 3){
      close(fd);
      break;
    }
  }

  if(argc > 1)
  { //user gave us a script to run (supposedly....)
    if((scriptfd = open(argv[1], O_RDONLY)) < 0)
    { //file was not found; never trust the user
      printf(2, "cannot open <%s>\n", argv[1]); //display error msg
      scriptfd = 0; //is -1 after failed open() attempt, so must change it back to 0
    }
  }

  setenvironment(1);
  // Read and run input commands.
  while(getcmd(buf, sizeof(buf)) >= 0){
    char b[7];
    slice(b, buf, 7);
    if(strcmp(b, "export ") == 0){
      struct execcmd* ecmd = (struct execcmd*)parsecmd(buf);
      if(ecmd->argv[0] == 0)
        exit(1);
      for (int i = 1;ecmd->argv[i]; ++i) {
      if (indexof(ecmd->argv[i], "=")) {
        setenv(ecmd->argv[i]);
      }
      }
      continue;
    } else if (buf[0] == 'e' && buf[1] == 'n' && buf[2] == 'v' && (buf[3] == '\n' || buf[3] == ' ')) {
      for (int j=0; env[j][0] != 0; ++j) {
        printf(1, "%s\n", env[j]);
      }
      continue;
    } else if(buf[0] == 'c' && buf[1] == 'd' && (buf[2] == ' ' || buf[2] == '\n')){
      // Chdir must be called by the parent, not the child.
      for (int i=0; env[i][0] != 0; ++i) {
        strcpy(envv[i], env[i]);
      }

      buf[strlen(buf)-1] = 0;  // chop \n
      if(chdir(buf+3) < 0)
        printf(2, "cannot cd %s\n", buf+3);

      setenvironment(0);
      for (int i=0; envv[i][0] != 0; ++i) {
        strcpy(env[i], envv[i]);
      }

      continue;
    } else if (buf[0] == 'c' && buf[1] == 'l' && buf[2] == 'e' && buf[3] == 'a' && buf[4] == 'r' && buf[5] == '\n') {
      ioctl(1, IOCTL_CLEAR, 0); // tell the console device to clear
      continue;
    }

    if(fork1() == 0)
      runcmd(parsecmd(buf));
    wait(&return_code);
  }
  return 0;
}

void
panic(char *s)
{
  printf(2, "%s\n", s);
  exit(-1);
}

int
fork1(void)
{
  int pid;

  pid = fork();
  if(pid == -1)
    panic("fork");
  return pid;
}

//PAGEBREAK!
// Constructors

struct cmd*
execcmd(void)
{
  struct execcmd *cmd;

  cmd = malloc(sizeof(*cmd));
  memset(cmd, 0, sizeof(*cmd));
  cmd->type = EXEC;
  return (struct cmd*)cmd;
}

struct cmd*
redircmd(struct cmd *subcmd, char *file, char *efile, int mode, int fd)
{
  struct redircmd *cmd;
  cmd = malloc(sizeof(*cmd));
  memset(cmd, 0, sizeof(*cmd));
  cmd->type = REDIR;
  cmd->cmd = subcmd;
  cmd->file = file;
  cmd->efile = efile;
  cmd->mode = mode;
  cmd->fd = fd;
  return (struct cmd*)cmd;
}

struct cmd*
pipecmd(struct cmd *left, struct cmd *right)
{
  struct pipecmd *cmd;

  cmd = malloc(sizeof(*cmd));
  memset(cmd, 0, sizeof(*cmd));
  cmd->type = PIPE;
  cmd->left = left;
  cmd->right = right;
  return (struct cmd*)cmd;
}

struct cmd*
listcmd(struct cmd *left, struct cmd *right)
{
  struct listcmd *cmd;

  cmd = malloc(sizeof(*cmd));
  memset(cmd, 0, sizeof(*cmd));
  cmd->type = LIST;
  cmd->left = left;
  cmd->right = right;
  return (struct cmd*)cmd;
}

struct cmd*
backcmd(struct cmd *subcmd)
{
  struct backcmd *cmd;

  cmd = malloc(sizeof(*cmd));
  memset(cmd, 0, sizeof(*cmd));
  cmd->type = BACK;
  cmd->cmd = subcmd;
  return (struct cmd*)cmd;
}
//PAGEBREAK!
// Parsing

char whitespace[] = " \t\r\n\v";
char symbols[] = "<|>&;()";

int
gettoken(char **ps, char *es, char **q, char **eq)
{
  char *s;
  int ret;

  s = *ps;
  while(s < es && strchr(whitespace, *s))
    s++;
  if(q)
    *q = s;
  ret = *s;
  switch(*s){
  case 0:
    break;
  case '|':
  case '(':
  case ')':
  case ';':
  case '&':
  case '<':
    s++;
    break;
  case '>':
    s++;
    if(*s == '>'){
      ret = '+';
      s++;
    }
    break;
  default:
    ret = 'a';
    while(s < es && !strchr(whitespace, *s) && !strchr(symbols, *s))
      s++;
    break;
  }
  if(eq)
    *eq = s;

  while(s < es && strchr(whitespace, *s))
    s++;
  *ps = s;
  return ret;
}

int
peek(char **ps, char *es, char *toks)
{
  char *s;

  s = *ps;
  while(s < es && strchr(whitespace, *s))
    s++;
  *ps = s;
  return *s && strchr(toks, *s);
}

struct cmd *parseline(char**, char*);
struct cmd *parsepipe(char**, char*);
struct cmd *parseexec(char**, char*);
struct cmd *nulterminate(struct cmd*);

struct cmd*
parsecmd(char *s)
{
  char *es;
  struct cmd *cmd;

  es = s + strlen(s);
  cmd = parseline(&s, es);
  peek(&s, es, "");
  if(s != es){
    printf(2, "leftovers: %s\n", s);
    panic("syntax");
  }
  nulterminate(cmd);
  return cmd;
}

struct cmd*
parseline(char **ps, char *es)
{
  struct cmd *cmd;

  cmd = parsepipe(ps, es);
  while(peek(ps, es, "&")){
    gettoken(ps, es, 0, 0);
    cmd = backcmd(cmd);
  }
  if(peek(ps, es, ";")){
    gettoken(ps, es, 0, 0);
    cmd = listcmd(cmd, parseline(ps, es));
  }
  return cmd;
}

struct cmd*
parsepipe(char **ps, char *es)
{
  struct cmd *cmd;

  cmd = parseexec(ps, es);
  if(peek(ps, es, "|")){
    gettoken(ps, es, 0, 0);
    cmd = pipecmd(cmd, parsepipe(ps, es));
  }
  return cmd;
}

struct cmd*
parseredirs(struct cmd *cmd, char **ps, char *es)
{
  int tok;
  char *q, *eq;

  while(peek(ps, es, "<>")){
    tok = gettoken(ps, es, 0, 0);
    if(gettoken(ps, es, &q, &eq) != 'a')
      panic("missing file for redirection");
    switch(tok){
    case '<':
      cmd = redircmd(cmd, q, eq, O_RDONLY, 0);
      break;
    case '>':
      cmd = redircmd(cmd, q, eq, O_WRONLY|O_CREATE, 1);
      break;
    case '+':  // >>
      cmd = redircmd(cmd, q, eq, O_WRONLY|O_CREATE, 1);
      break;
    }
  }
  return cmd;
}

struct cmd*
parseblock(char **ps, char *es)
{
  struct cmd *cmd;

  if(!peek(ps, es, "("))
    panic("parseblock");
  gettoken(ps, es, 0, 0);
  cmd = parseline(ps, es);
  if(!peek(ps, es, ")"))
    panic("syntax - missing )");
  gettoken(ps, es, 0, 0);
  cmd = parseredirs(cmd, ps, es);
  return cmd;
}

struct cmd*
parseexec(char **ps, char *es)
{
  char *q, *eq;
  int tok, argc;
  struct execcmd *cmd;
  struct cmd *ret;

  if(peek(ps, es, "("))
    return parseblock(ps, es);

  ret = execcmd();
  cmd = (struct execcmd*)ret;

  argc = 0;
  ret = parseredirs(ret, ps, es);
  while(!peek(ps, es, "|)&;")){
    if((tok=gettoken(ps, es, &q, &eq)) == 0)
      break;
    if(tok != 'a')
      panic("syntax");
    cmd->argv[argc] = q;
    cmd->eargv[argc] = eq;
    argc++;
    if(argc >= MAXARGS)
      panic("too many args");
    ret = parseredirs(ret, ps, es);
  }
  cmd->argv[argc] = 0;
  cmd->eargv[argc] = 0;
  return ret;
}

// NUL-terminate all the counted strings.
struct cmd*
nulterminate(struct cmd *cmd)
{
  int i;
  struct backcmd *bcmd;
  struct execcmd *ecmd;
  struct listcmd *lcmd;
  struct pipecmd *pcmd;
  struct redircmd *rcmd;

  if(cmd == 0)
    return 0;

  switch(cmd->type){
  case EXEC:
    ecmd = (struct execcmd*)cmd;
    for(i=0; ecmd->argv[i]; i++)
      *ecmd->eargv[i] = 0;
    break;

  case REDIR:
    rcmd = (struct redircmd*)cmd;
    nulterminate(rcmd->cmd);
    *rcmd->efile = 0;
    break;

  case PIPE:
    pcmd = (struct pipecmd*)cmd;
    nulterminate(pcmd->left);
    nulterminate(pcmd->right);
    break;

  case LIST:
    lcmd = (struct listcmd*)cmd;
    nulterminate(lcmd->left);
    nulterminate(lcmd->right);
    break;

  case BACK:
    bcmd = (struct backcmd*)cmd;
    nulterminate(bcmd->cmd);
    break;
  }
  return cmd;
}
